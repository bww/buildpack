#!/usr/bin/env bash

set -e

# where am i
ME="$0"
ME_HOME=$(dirname "$0")
ME_HOME=$(cd "$ME_HOME" && pwd)

# note the install base
if [ -z "$1" ]; then
  echo "No installation base is defined"
  exit -1
else
  install_base="$1"
  shift
fi

# note the repository
if [ -z "$1" ]; then
  echo "No repository URL is defined"
  exit -1
else
  repo_url="$1"
  shift
fi

# note the deploy target
if [ -z "$1" ]; then
  echo "No deploy target is defined"
  exit -1
else
  deploy_target="$1"
  shift
fi

# repo name
repo_name=$(basename "$repo_url" | sed 's/\.[^\.]*$//')

# setup our roots
install_usr="$install_base/usr/svc"
install_var="$install_base/var/svc"
install_etc="$install_base/etc/svc"

# make sure our /usr root exist
if [ ! -d "$install_usr" ]; then
  mkdir -p "$install_usr"
fi

# make sure our /var root exist
if [ ! -d "$install_var/$repo_name" ]; then
  mkdir -p "$install_var/$repo_name"
fi

# make sure our /etc root exist
if [ ! -d "$install_etc/$repo_name" ]; then
  mkdir -p "$install_etc/$repo_name"
fi

# canonicalize our roots and remove extraneous slashes
install_usr=$(cd "$install_usr" && pwd | sed s#//*#/#g)
install_var=$(cd "$install_var" && pwd | sed s#//*#/#g)
install_etc=$(cd "$install_etc" && pwd | sed s#//*#/#g)

# repo path
repo_path="$install_usr/$repo_name"

# environment config
project_env="$install_etc/$repo_name/env"
# setup environment config if we haven't already
if [ ! -f "$project_env" ]; then
  echo "# environment configuration for $repo_name" > "$project_env"
  chmod 600 "$project_env"
fi

# display some info
echo "-----> $repo_name <$deploy_target> $repo_url"
echo "-----> $install_base"

# pull down the repository or update it
if [ -d "$repo_path" ]; then
  (cd "$repo_path" && git pull)
else
  (cd "$install_usr" && git clone "$repo_url" "$repo_name")
fi

# find our procfile
if [ -f "$repo_path/procfile" ]; then
  procfile_path="$repo_path/procfile"
elif [ -f "$repo_path/Procfile" ]; then
  procfile_path="$repo_path/Procfile"
fi

# make sure we have a procfile
if [ -z "$procfile_path" ]; then
  echo "No procfile found"
  exit -1
fi

# find our deployment target
while read line; do
  target_key=$(echo "$line" | sed 's/:.*$//')
  
  if [ -z "$all_targets" ]; then
    all_targets="$target_key"
  else
    all_targets="$all_targets, $target_key"
  fi
  
  if [ "$target_key" = "$deploy_target" ]; then
    run_command=$(echo "$line" | sed 's/[^:]*://')
    break
  fi
  
done < "$procfile_path"

# make sure we found our runner command
if [ -z "$run_command" ]; then
  echo "No target named '$deploy_target' in ($all_targets)"
  exit -1
fi

set +e

# export our build environment
export MESS_BUILDPACK_HOME="$ME_HOME"
export MESS_BUILDPACK_DEPLOY_ROLE="$deploy_target"
export MESS_BUILDPACK_DEPLOY_TARGET="$deploy_target"
export MESS_BUILDPACK_REPO_URL="$repo_url"
export MESS_BUILDPACK_USR="$install_usr/$repo_name"
export MESS_BUILDPACK_VAR="$install_var/$repo_name"
export MESS_BUILDPACK_ETC="$install_etc/$repo_name"
export MESS_BUILDPACK_RESOURCES="$MESS_BUILDPACK_VAR/resources"

# make sure the resources directory exists
if [ ! -d "$MESS_BUILDPACK_RESOURCES" ]; then
  mkdir -p "$MESS_BUILDPACK_RESOURCES"
fi

# display our build command
echo "-----> <$deploy_target> $run_command"
# build our project
(cd "$repo_path" && bash -c "$run_command")

# report success or failure
if [ $? -ne "0" ]; then
  echo "-----> <$deploy_target> FAILED"
else
  echo "-----> <$deploy_target> SUCCESS"
fi

