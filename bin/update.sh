#!/usr/bin/env bash

set -e

# where am i
ME="$0"
ME_HOME=$(dirname "$0")
ME_HOME=$(cd "$ME_HOME" && pwd)

# default update role
update_target=""
# default instance role
instance_role=""

# parse arguments
args=$(getopt r:t:s: $*)
# set arguments
set -- $args
# progess arguments
for i; do
  case "$i"
  in
    -r)
      instance_role="$2"; shift;
      shift;;
    -t)
      update_target="$2"; shift;
      shift;;
    -s)
      target_service="$2"; shift;
      shift;;
    --)
      shift; break;;
  esac
done

# make sure we have an update target
if [ -z "$update_target" ]; then
  echo "No update target is provided (-t <target>)"
  exit -1
fi

# make sure we have an instance role
if [ -z "$instance_role" ]; then
  echo "No instance role is provided (-r <role>)"
  exit -1
fi

# make sure we have a target service
if [ -z "$target_service" ]; then
  echo "No target service is provided (-s <service>)"
  exit -1
fi

# note the repository URL
if [ -z "$1" ]; then
  echo "No repository URL is provided"
  exit -1
else
  repo_url="$1"
  shift
fi

# do the update
"$ME_HOME/foreach.sh" "$instance_role" <<END
  sudo service "$target_service" stop
  set -e
  (cd buildpack && git pull) # self-update
  ./buildpack/bin/deploy.sh -t "$update_target" "$repo_url"
  sudo service "$target_service" start
END

