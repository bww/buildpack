#!/usr/bin/env ruby
# 
# EC2 instance lookup script. This script queries EC2 for instances matching certain
# criteria and outputs a list of hostnames for matching instances.
# 

require 'rubygems'
require 'optparse'
require 'aws-sdk'

config = File.new("#{Dir.home}/.aws/config", "r")

while (line = config.gets)
  if (match = /([\w\d]+)\s*=\s*(.*)/.match(line)) != nil
    case match[1]
    when 'aws_access_key_id'
      AWS_KEY = match[2]
    when 'aws_secret_access_key'
      AWS_SECRET = match[2]
    when 'region'
      AWS_REGION = match[2]
    end
  end
end

config.close

AWS.config(access_key_id: AWS_KEY, secret_access_key: AWS_SECRET, region: AWS_REGION)

usage = "Usage: #{File.basename($0)} [options]\n Help: #{File.basename($0)} --help"
options = { }

begin
  
  OptionParser.new { |opts|
    
    opts.banner = "Usage: #{File.basename($0)} [options]"
    opts.separator "\nOptions:"
    
    opts.on('-1', '--one', 'Display only a single instance (the first).') do
      options[:single] = true;
    end
    
    opts.on('-r', '--role role', 'The instance role to fetch.') do |v|
      options[:role] = v;
    end
    
    opts.on('-u', '--user user', 'Prefix instance hostnames with the provided username.') do |v|
      options[:user] = v;
    end
    
    opts.on('-v', '--verbose', 'Be more verbose.') do
      options[:verbose] = true;
    end
    
    opts.on_tail(nil, '--help', 'Display this help message.') do
      puts "#{opts}\n"; exit
    end
    
  }.parse!
  
rescue OptionParser::ParseError => error
  puts "#{error}\n#{usage}"
  exit -1
end

def instance_tag_value(instance, name)
  instance.tags.each do |k, v|
    if k == name
      return v
    end
  end
end

def instance_display_name(instance, default_name = '')
  if (name = instance_tag_value(instance, 'Name')) != nil
    return name
  else
    return default_name;
  end
end

ec2 = AWS::EC2.new

instances = [ ]

ec2.instances.inject(instances) { |m, i|
  if i.status == :running
    if options[:role] == nil
      instances.push(i);
    elsif (tags = i.tags) != nil
      tags.each do |k, v|
        if options[:role] == nil || (k == 'system.role' && v == options[:role])
          instances.push(i);
        end
      end
    end
  end
}

w_name = 0;

if options[:verbose]
  for instance in instances
    w_name = [w_name, instance_display_name(instance).length].max
  end
end

for instance in instances
  
  if options[:verbose]
    printf "%-#{w_name}s : ", instance_display_name(instance)
  end
  
  if options[:user] != nil
    puts "#{options[:user]}@#{instance.dns_name}"
  else
    puts instance.dns_name
  end
  
  if options[:single] == true
    break
  end
  
end

