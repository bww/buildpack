#!/usr/bin/env bash
# 
# Execute a command on all nodes of a particular role
# 

set -e

# where am i
ME="$0"
ME_HOME=$(dirname "$0")
ME_HOME=$(cd "$ME_HOME" && pwd)

# instances
INSTANCES="$ME_HOME/instances.rb"

# make sure we have a role specified
if [ ! -z "$1" ]; then
  ROLE="$1"; shift
else
  echo "usage: $0 <role>"; exit -1
fi

# obtain the hosts we're operating on
HOSTS=$("$INSTANCES" -r "$ROLE" -u "ubuntu")
# pull in our script
script=$(cat)

# run our script on each host
for host in $HOSTS; do
  ssh "${host}" <<END
  $script
END
done

