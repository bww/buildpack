#!/usr/bin/env bash

set -e

# where am i
ME="$0"
ME_HOME=$(dirname "$0")
ME_HOME=$(cd "$ME_HOME" && pwd)

# default deploy prefix
install_base="/"
# default deploy target
deploy_target=""

# parse arguments
args=$(getopt p:t: $*)
# set arguments
set -- $args
# progess arguments
for i; do
  case "$i"
  in
    -p)
      install_base="$2"; shift;
      shift;;
    -t)
      deploy_target="$2"; shift;
      shift;;
    --)
      shift; break;;
  esac
done

# note the repository URL
if [ -z "$1" ]; then
  echo "No repository URL is provided"
  exit -1
else
  repo_url="$1"
  shift
fi

# do the build
sudo "$ME_HOME/build.sh" "$install_base" "$repo_url" "$deploy_target"

